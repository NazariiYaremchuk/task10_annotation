package com.yaremchuk.model;

import java.util.Arrays;

public class MyClass {
    @Info(name = "Nazar")
    public int i = 1;
    @Info(name = "Mykola", age = 25)
    int j = 2;
    int k = 3;

    String customField = "Hello World";

    public String getName() throws NoSuchFieldException {
        return MyClass.class.getField("i").getAnnotation(Info.class).toString();
    }

    //task
    public String testMethod() {
        return "\nCalled method without parameters\n";
    }

    public int testMethod(int... args) {
        return args.length;
    }

    public String testMethod(String s, int... args) {
        return s + Arrays.toString(args);
    }

    public String testMethod(String s, String... args) {
        return s + Arrays.toString(args);
    }

    public <T> String showInfo(T obj) {
        return obj.getClass().toString() + "; value = " + obj.toString();
    }
}

package com.yaremchuk.model;

@FunctionalInterface
public interface Printable {
    void print();
}
